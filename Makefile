# Go parameters
GOCMD      := go
GOBUILD    := GO111MODULE=on $(GOCMD) build -v -mod=vendor
GOCLEAN    := $(GOCMD) clean
GOTEST     := GO111MODULE=on $(GOCMD) test -v
GOGET      := GO111MODULE=on $(GOCMD) get -v
GOVET      := GO111MODULE=on $(GOCMD) vet
GORACE     := $(GOCMD) race
GOTOOL     := GO111MODULE=on $(GOCMD) tool
GOLINT     := golint

SRCS       := $(shell find . -name "*.go" -type f ! -path "./vendor/*" ! -path "*/bindata.go")
VERSION    := $(shell git describe --always --long --dirty 2>/dev/null)

EXECUTABLE := testbench

export GO111MODULE=off

PACKAGES ?= $(shell GO111MODULE=on $(GOCMD) list -mod=vendor ./... | grep -v /vendor/)

ifndef VERBOSE
.SILENT:
endif

.DEFAULT_GOAL := help

all: dep build test coverage  ## Run all for development

dep: ## Get all the dependencies
		@echo "Getting dependencies"
		$(GOGET) -d ./...

fmt: ## Format source code
		GO111MODULE=on $(GOCMD) fmt $(PACKAGES)

test: ## Running tests
		@echo "Running tests"
		@echo "============="
		@echo "Running $(GOVET)"
		$(GOVET) ./.
		@echo "Running $(GOTEST) race condition"
		$(GOTEST) -race ./.
		@echo "Running $(GOTEST) race memory sanity"
		CC=clang $(GOTEST) -msan ./.
		@echo "Running tests"
		$(GOTEST) ./. || exit 1;
		@echo "Running benchs"
		$(GOTEST) -bench ./.
		@echo ""

build: ## Building development binary
		@echo "Running $(GOBUILD)"
		@echo "================"
		$(GOBUILD) -tags development -ldflags '$(DEVLDFL) $(LDFLAGS)' ./. ;
		@echo ""

coverage: ## Generating coverage for files
		@echo "Running coverage"
		@echo "================"
		$(GOTEST) ./... -coverpkg=./... -coverprofile coverage.cov
		$(GOTOOL) cover -func=coverage.cov

clean: ## Cleaning up
		@echo "Cleaning package"
		@echo "================"
		rm -f ${EXECUTABLE}

help:  ## Displaying help for build targets
		@echo "Available targets in this makefile:"
		@echo ""
		@grep -E '^[ a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | \
		awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-15s\033[0m %s\n", $$1, $$2}'
