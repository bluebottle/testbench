Testing if packages install under different python versions with go.

Usage:

- Move testbench.yml to /etc
- Substitute "&lt;ACCESSTOKEN&gt;" with a valid access token
- Substitute "&lt;PROJECTID&gt;" with a valid project id number

- Run the program:

<pre>
$ ./testbench
Usage: testbench [OPTIONS]

-a --attach  &lt;PID&gt;    Attach to a running pipeline
-e --extra   &lt;string&gt; Extra packages to install
-h --help             Show this help message and exit
-p --package &lt;string&gt; Package to test
-u --use     &lt;string&gt; Extra USE flags
$
</pre>
