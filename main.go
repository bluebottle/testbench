package main

import (
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/spf13/viper"
	"github.com/urfave/cli/v2"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/bluebottle/go-modules/misc"
)

// Usage displays program usage.
var Usage = func() {
	fmt.Println(`Usage: testbench [OPTIONS]

-a --attach  <PID>    Attach to a running pipeline
-e --extra   <string> Extra packages to install
-h --help             Show this help message and exit
-p --package <string> Package to test
-u --use     <string> Extra USE flags`)
} // usage ()

// Checking if the pipeline is finished
func pipelineFinished(git *gitlab.Client, pid string, pipeID int) (bool, string) {
	pipeline, _, err := git.Pipelines.GetPipeline(pid, pipeID)

	if err != nil {
		fmt.Println("gitlab-ci error: ", err.Error())
	} else {
		if pipeline.Status != "pending" && pipeline.Status != "running" {
			return true, pipeline.Status
		} // if
	} // if

	return false, pipeline.Status
} // pipelineFinished ()

func fmtDuration(d time.Duration) string {
	d = d.Round(time.Second)
	h := d / time.Hour
	d -= h * time.Hour
	m := d / time.Minute
	d -= m * time.Minute
	s := d / time.Second
	return fmt.Sprintf("%02d:%02d:%02d", h, m, s)
} // fmtDuration ()

func main() {
	var attach int64
	var extra, paket, use string

	app := &cli.App{
		Name:                   "testbench",
		Usage:                  "Testing if packages install under different python versions with go.",
		UseShortOptionHandling: true,
		Version:                "0.0.2",
		Flags: []cli.Flag{
			&cli.Int64Flag{
				Name:        "attach",
				Aliases:     []string{"a"},
				Usage:       "Attach to a running pipeline",
				Destination: &attach,
			},
			&cli.StringFlag{
				Name:        "extra",
				Aliases:     []string{"e"},
				Usage:       "Extra packages to install",
				Destination: &extra,
			},
			&cli.StringFlag{
				Name:        "paket",
				Aliases:     []string{"p"},
				Usage:       "Package to test",
				Destination: &paket,
			},
			&cli.StringFlag{
				Name:        "use",
				Aliases:     []string{"u"},
				Usage:       "Extra USE flags",
				Destination: &use,
			},
		},
		Action: func(c *cli.Context) error {
			if paket != "" {
				mainwork(attach, paket, use, extra)
			} else {
				fmt.Println(c.App.Usage)
			} // if

			return nil
		},
	}

	err := app.Run(os.Args)
	misc.ShowError(err, "", "ErrFatal")
}

func mainwork(attach int64, paket, use, extra string) {
	var (
		jobStat  map[string]int
		resp     *gitlab.Response
		pipeline *gitlab.Pipeline
	)

	v := viper.New()
	v.SetConfigName("testbench")
	v.AddConfigPath("/etc")
	v.AddConfigPath(".")
	v.AutomaticEnv()
	err := v.ReadInConfig()

	if err != nil {
		fmt.Println(err)
	} // if

	// Get access token and project id fron config
	token := v.GetStringMapString("global")["token"]
	pid := v.GetStringMapString("global")["projectid"]

	//
	// TODO: Put creation, starting and further handling into functions?
	//
	// create client
	git, err := gitlab.NewClient(token)
	misc.ShowError(err, "", "ErrFatal")

	// Create new pipeline if we don't want to attach to one
	if attach == 0 {
		var gitVar []*gitlab.PipelineVariable
		gitVar = append(gitVar, &gitlab.PipelineVariable{Key: "Package", Value: paket, VariableType: "env_var"})
		gitVar = append(gitVar, &gitlab.PipelineVariable{Key: "USE", Value: use, VariableType: "env_var"})
		gitVar = append(gitVar, &gitlab.PipelineVariable{Key: "Extra", Value: extra, VariableType: "env_var"})

		pipeOpt := &gitlab.CreatePipelineOptions{Ref: gitlab.String("master"), Variables: &gitVar}
		pipeline, resp, err = git.Pipelines.CreatePipeline(pid, pipeOpt)
	} else {
		pipeline, resp, err = git.Pipelines.GetPipeline(pid, int(attach))
	} // if

	if err != nil {
		fmt.Print("Response: ")
		fmt.Println(resp)
		fmt.Print("Error: ")
		fmt.Println(err)
	} else {
		// Wait for the pipeline to finish
		pipFinish, pipStat := pipelineFinished(git, pid, pipeline.ID)
		jobOpts := &gitlab.ListJobsOptions{}
		started := time.Now()

		for !pipFinish {
			jobList, resp, err := git.Jobs.ListPipelineJobs(pid, pipeline.ID, jobOpts)
			if err != nil {
				fmt.Print("Response: ")
				fmt.Println(resp)
				fmt.Print("Error: ")
				fmt.Println(err)
			} else {
				jobStat = map[string]int{
					"created": 0,
					"running": 0,
					"pending": 0,
					"success": 0,
					"failed":  0,
				}

				for _, v := range jobList {
					jobVal, _, err := git.Jobs.GetJob(pid, v.ID)

					if err == nil {
						jobStat[jobVal.Status]++
					} else {
						fmt.Println(err)
					} // if
				} // for
			} // if

			fmt.Print(fmtDuration(time.Since(started)) + ": Pipeline running (Status " + pipStat + ") ")
			fmt.Print("c(" + strconv.Itoa(jobStat["created"]) + ") ")
			fmt.Print("f(" + strconv.Itoa(jobStat["failed"]) + ") ")
			fmt.Print("p(" + strconv.Itoa(jobStat["pending"]) + ") ")
			fmt.Print("r(" + strconv.Itoa(jobStat["running"]) + ") ")
			fmt.Print("s(" + strconv.Itoa(jobStat["success"]) + ") ")
			fmt.Println(". Waiting 60 seconds.")
			time.Sleep(60 * time.Second)
			pipFinish, pipStat = pipelineFinished(git, pid, pipeline.ID)
		} // for

		pipeline, _, err := git.Pipelines.GetPipeline(pid, pipeline.ID)

		if err != nil {
			fmt.Println(err)
		} // if

		duration := pipeline.Duration
		minutes := strconv.FormatInt(int64(duration/60), 10)
		seconds := strconv.FormatInt(int64(duration%60), 10)
		fmt.Println("Pipeline finished after " + minutes + " minutes " + seconds + " seconds with status " + pipeline.Status)
		//
		// TODO: Get results of pipeline jobs in preparation to get them displayed
		//
	} // if
} // main ()
