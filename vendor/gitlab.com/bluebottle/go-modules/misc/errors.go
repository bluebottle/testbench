package misc

import (
	"log"
)

// ShowError Displays errors depending on the type of error
// Error types:
// ErrPanic    - display error and panic
// MsgPanic    - display message and panic
// ErrMsgPanic - display error and message and panic
// ErrFatal    - display error and exit
// MsgFatal    - display message and exit
// ErrMsgFatal - display error and message and exit
// ErrPrint    - display error
// MsgPrint    - display message
// ErrMsgPrint - display error and message
func ShowError(e error, msg, errType string) {
	switch errType {
	case "ErrPanic":
		if e != nil {
			log.Panic(e)
		} // if
	case "MsgPanic":
		log.Panic(msg)
	case "ErrMsgPanic":
		ifPanic(e, msg)
	case "ErrFatal":
		if e != nil {
			log.Fatal(e)
		} // if
	case "MsgFatal":
		log.Fatal(msg)
	case "ErrMsgFatal":
		ifFatal(e, msg)
	case "ErrPrint":
		if e != nil {
			log.Println(e)
		} // if
	case "MsgPrint":
		log.Println(msg)
	case "ErrMsgPrint":
		ifPrint(e, msg)
	default:
		log.Println("Error type " + errType + " not valid")
	} // switch
} // ShowError ()

// ifPanic panics with the error and message, if there is an error
func ifPanic(e error, msg string) {
	if e != nil {
		log.Panic(e.Error() + " " + msg)
	} // if
} // ifPanic ()

// ifFatal fails with the error and message, if there is an error
func ifFatal(e error, msg string) {
	if e != nil {
		log.Fatal(e.Error() + " " + msg)
	} // if
} // ifFatal ()

// ifPrint prints the error and message, if there is an error
func ifPrint(e error, msg string) {
	if e != nil {
		log.Println(e.Error() + " " + msg)
	} // if
} // ifPrint ()
